#include <fcntl.h>
#include <termios.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <errno.h>

int uart_filestream = 1;
  
static void *uart_read_thread(void *arg)
{
  char buffer[100];
  ssize_t res = 0;

  for(;;)
  {
    memset(buffer, 0x00, sizeof(buffer));
    res = read(uart_filestream, buffer, sizeof(buffer));
    if(res < 0)
    {
      printf("Error read date!!!\r\n");
    }

    printf("Receive %d -  bytes %s\n", res, buffer); 

    if(res == 1 && buffer[0] == 'q')
      break;
    
    res = write(uart_filestream, buffer, strlen(buffer));
    if (res == -1) 
    {
      perror("write serial port");
      exit(EXIT_FAILURE);
    } 
  }

  return NULL;
}

static void open_uart_stream(void)
{
  uart_filestream = open("/dev/ttyAMA0", O_RDWR | O_NOCTTY);

  if(uart_filestream == -1)
    printf("Error - Unable to open UART\n");


  struct termios options;

  tcgetattr(uart_filestream, &options);
  options.c_cflag = B115200 | CS8 | CLOCAL | CREAD;  // Set baud rate
  options.c_iflag = IGNPAR;
  options.c_oflag = 0;
  options.c_lflag = 0;
  tcflush(uart_filestream, TCIFLUSH);
  tcsetattr(uart_filestream, TCSANOW, &options);
}

int main(int argv, char* argc[])
{
  pthread_t uart_thread;
  void *res;

  open_uart_stream();

  if(pthread_create(&uart_thread, NULL, uart_read_thread, NULL) != 0)
  {
    printf("Error open thread %d\n", errno);
    exit(EXIT_FAILURE);
  }

  if(pthread_join(uart_thread, &res) != 0)
  {
    printf("Error thread join\n");
    exit(EXIT_FAILURE);
  }
  
  close(uart_filestream);

  return 0;
}
