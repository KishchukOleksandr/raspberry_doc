import serial, time
import RPi.GPIO as GPIO

print("UART test start")

ser = serial.Serial(port = "/dev/ttyAMA0" , baudrate = 9600 , timeout = 2 )
if (ser.isOpen() == False):
    ser.open()
ser.flushInput()
ser.flushOutput()

echostring = "Talking to ourselves!\r\n"
try:
    def echotest():
        while (True):
            ser.flushInput()
            ser.flushOutput()
            ser.write(echostring.encode())
            print ('What we sent: ', echostring)
            time.sleep(0.5)
            echostringrec = ser.readline().decode("utf-8")
            print ('What we received: ', echostringrec)

    def destroy():
        ser.close()
        print ("test complete")
except:
    print("Error!!!")

echotest()



